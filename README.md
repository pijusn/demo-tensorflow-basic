# Basic demo for TesnsorFlow library
Demo is based on TensorFlow's basic level introduction. It is extended with functionality needed to visualize the process.

# Requirements
This demo depends on following libraries being installed:

- tensorflow
- matplotlib
- numpy
