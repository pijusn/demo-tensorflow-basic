import utils.mnist as mnist
import utils.images as images

if __name__ == '__main__':
    limit = 15

    for it in mnist.read(path = "MNIST_data"):
        images.showGreyscale([it[1]])
        limit -= 1
        if limit <= 0:
            break
