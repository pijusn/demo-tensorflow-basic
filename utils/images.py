from matplotlib import pyplot
import matplotlib as mpl
import math

import numpy as np

def saveFiltersImage(weights, filename):
    def digit(n):
        return _filterAsRGB(np.reshape(weights[:,n], (28, 28)))

    _saveRGBMultiPlot([
        digit(0),
        digit(1),
        digit(3),
        digit(4),
        digit(5),
        digit(6),
        digit(7),
        digit(8),
        digit(9)
    ], filename)

def showGreyscale(bytes):
    """
    Shows greyscale image in a new window.
    """

    fig = _multiPlotFigure(bytes, cmap=mpl.cm.Greys)
    fig.show()
    pyplot.show()
    pyplot.close(fig)

def _saveGreyscaleMultiPlot(bytes, filename):
    """
    Saves greyscale image to give file.
    """

    fig = _multiPlotFigure(bytes, cmap=mpl.cm.Greys)
    fig.savefig(filename)
    pyplot.close(fig)

def _saveRGBMultiPlot(bytes, filename):
    """
    Saves RGB image to give file.
    """
    fig = _multiPlotFigure(bytes)
    fig.savefig(filename)
    pyplot.close(fig)

def _filterAsRGB(weights):
    rows, columns = weights.shape
    ret = np.empty((rows, columns, 3), dtype=np.uint8)
    ret[:, :, :] = 255.0

    for r in range(0, rows):
        for c in range(0, columns):
            if weights[r, c] < 0:
                x = max(-1, weights[r, c]) * -255
                ret[r, c, 1] = 255 - x
                ret[r, c, 2] = 255 - x
            else:
                x = min(1, weights[r, c]) * 255
                ret[r, c, 0] = 255 - x
                ret[r, c, 1] = 255 - x
                ret[r, c, 2] = 255 - x

    return ret

def _multiPlotFigure(bytes, cmap=None):
    """
    Saves greyscale image to give file.
    """

    fig = pyplot.figure()
    count = len(bytes)

    rows = math.ceil(count / 3)
    columns = 3

    for i in range(0, count):
        ax = fig.add_subplot(rows, columns, i + 1)
        imgplot = ax.imshow(bytes[i], cmap=cmap)
        imgplot.set_interpolation('nearest')
        ax.xaxis.set_ticks_position('top')
        ax.yaxis.set_ticks_position('left')

    return fig
