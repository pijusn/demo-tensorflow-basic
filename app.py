from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf

import utils.fs as fs
import utils.images as images

fs.mkdir('MNIST_data')
fs.mkdir('filters')

# Read MNIST data.
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

# Initialize model.
x = tf.placeholder(tf.float32, [None, 784])
w0 = tf.Variable(tf.zeros([784, 10]))

l0 = tf.matmul(x, w0)
y = tf.nn.softmax(l0)

y_ = tf.placeholder(tf.float32, [None, 10])

# Training configuration.
cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))
train_step = tf.train.GradientDescentOptimizer(1.0).minimize(cross_entropy)

# Initialize session.
sess = tf.Session()
sess.run(tf.initialize_all_variables())

def save(session, iterationCounter):
    saver.save(session, "saves/model-%s.ckpt" % iterationCounter)

def evaluate():
    correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    return sess.run(accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels})

def checkpoint(index):
    print("#%d Accuracy: %s" % (index, evaluate()))
    images.saveFiltersImage(sess.run(w0), "filters/%d.png" % (index))

def train():
    batch_xs, batch_ys = mnist.train.next_batch(100)
    sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})

def main():
    checkpoint(0)

    for i in range(10000):
        train()
        if (i + 1) % 100 == 0:
            checkpoint(i + 1)

if __name__ == "__main__":
    main()
